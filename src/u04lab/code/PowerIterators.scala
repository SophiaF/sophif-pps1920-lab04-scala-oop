package u04lab.code

import Optionals._
import Lists._
import u04lab.code.Streams._
import u04lab.code.Lists.List.Cons
import u04lab.code.Lists.List.Nil
import u04lab.code.Lists.List._
import u04lab.code.Streams.Stream._

/**
 * An interface modelling an iterator of elements of type T,
 * with methods to "recall" what happened in the past.
 * Note that it can also model an infinite stream of elements
 *
 */
trait PowerIterator[A] {
  /**
   * @return an empty optional if there are no more elements, otherwise it gives an optional
   * filled with the next element
   */
  def next(): Option[A]
  /**
   * @return the list of elements produced so far, without altering the state of iteration
   */
  def allSoFar(): List[A]
  /**
   * @return a new iterator, to get all elements already produced, in reversed order.
   * It does not alter the state of this iteration
   * (this part is optional in this exam)
   */
  def reversed(): PowerIterator[A]
}

class PowerIteratorsImpl[A] (var elements: Stream[A]) extends PowerIterator[A] {
  elements = empty()
  /**
   * @return an empty optional if there are no more elements, otherwise it gives an optional
   *         filled with the next element
   */
  override def next(): Option[A] = ???

  /**
   * @return the list of elements produced so far, without altering the state of iteration
   */
  override def allSoFar(): List[A] = ???

  /**
   * @return a new iterator, to get all elements already produced, in reversed order.
   *         It does not alter the state of this iteration
   *         (this part is optional in this exam)
   */
  override def reversed(): PowerIterator[A] = ???
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  /**
   * @return an iterator over the infinite sequence that begins with 'start', and applies 'successive' each time
   * recall that you compute the next of 'i' with instruction 'successive.apply(i)'
   */
  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = ???

  /**
   * @param list
   * @return an iterator over the element of the input list
   */
  override def fromList[A](list: List[A]): PowerIterator[A]= ???

  /**
   * @return an iterator over a sequence of random booleans, with length size
   * such a sequence should be produced "by need", since size could be very large
   */
  override def randomBooleans(size: Int): PowerIterator[Boolean] = ???
}
